# Use case 1: As a user I want to upload and name an image.

## Scenario 1:

* Given image named "blue" do not exist
* When I access page with upload form
* And I set "blue" in name field
* And I click select button
* And I select a image from the system
* Then I see a confirmation message "blue image uploaded" And I can access the image in /img/blue url

## Scenario 2:

* Given image named "blue" do exist
* When I access page with upload form
* And I set "blue" in name field
* And I click select button
* And I select a image from the system
* Then I see a confirmation message "blue image changed" And I can access the new image in /img/blue url

# Use case 2: As a user I want to access a named image.

## Scenario 1:

* Given image named "blue" do not exist When I access page /image/blue
* Then I see a 404 page

## Scenario 2:

* Given image named "blue" do exist
* When I access page /image/blue
* Then I see the "blue" image with the text "blue image" on top
